<?php
namespace App\Controllers;

defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\User as Users,
    \Core\Controller,
    \App\Libraries\ConsumerPaypal;

class Home extends Controller
{

    public function index()
    {
        $paypal = new ConsumerPaypal();
        echo "<pre>";
        print_r($paypal);
        //View::set("msg", "Hello World!!");
        //View::render("home/index");
    }

    public function create_credit_card()
    {
        $paypalTest = new ConsumerPaypal();
        $card = $paypalTest->saveCard();
        echo "<pre>";
        print_r($card->toArray());
    }

    public function find_credit_card()
    {
        $paypalTest = new ConsumerPaypal();
        $card = $paypalTest->find_card("CARD-4BK16991F0473131RLIUE7HA");
        echo "<pre>";
        print_r($card->toArray());
    }

    public function payment_with_credit_card()
    {
        $paypalTest = new ConsumerPaypal();
        $payment_with_credit_card = $paypalTest->savePaymentWithExistingVisa("CARD-4BK16991F0473131RLIUE7HA");
        echo "<pre>";
        print_r($payment_with_credit_card->toArray());
    }

    public function create_payment_with_paypal()
    {
        $paypal = new ConsumerPaypal();
        $approvalUrl = $paypal->savePaymentWithPaypal();
        echo "<a href='".$approvalUrl."'>Pagar con paypal</a>";
    }

    public function get_payment_with_paypal()
    {
        $paypalTest = new ConsumerPaypal();
        $get_payment_with_paypal = $paypalTest->getPaymentWithPayPal();
        echo "<pre>";
        print_r($get_payment_with_paypal->toArray());
    }

    public function refund_payment_with_paypal()
    {
        $paypalTest = new ConsumerPaypal();
        $refund_payment_with_paypal = $paypalTest->refundPaymentWithPayPal();
        echo "<pre>";
        print_r($refund_payment_with_paypal->toArray());
    }

    public function void_payment_with_paypal(){
        $paypalTest = new ConsumerPaypal();
        $void_payment_with_paypal = $paypalTest->voidPaymentWithPayPal();
        echo "<pre>";
        print_r($void_payment_with_paypal->toArray());
    }

    public function paypal_payment_response($res)
    {
        if($res == true)
        {
            $paymentId = $_GET["paymentId"];
            $token = $_GET["token"];
            $payerId = $_GET["PayerID"];
            echo sprintf(
                'PaymentID: %s <br>Token: %s<br>PayerID: %s',
                $paymentId, $token, $payerId
            );
     
            $paypal = new ConsumerPaypal();
            $payment = $paypal->execute_payment($paymentId, $payerId);
     
            echo "<pre>";
            print_r($payment->toArray());
        }
        else 
        {
            echo "Payment failed";
        }
    }

    /**
     * [test description]
     * @param  [type] $user [description]
     * @param  [type] $age  [description]
     * @return [type]       [description]
     */
    public function test($user, $age)
    {
        View::set("user", $user);
        View::set("title", "Custom MVC");
        View::render("home");
    }

    public function admin($name)
    {
        $users = Users::getAll();
        View::set("users", $users);
        View::set("title", "Custom MVC");
        View::render("admin");
    }

    public function user($id = 1)
    {
        $user = Users::getById($id);
        print_r($user);
    }
}
